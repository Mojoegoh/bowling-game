﻿using NUnit.Framework;
using System;
using BowlingGame;
namespace BowlingGameTest
{
    [TestFixture()]
    public class Test
    {
        public Game game;
        [SetUp]
        public void SetUpGame()
        {
            game = new Game();
        }

        [Test]
        public void CheckGameObject()
        {
            game = new Game();
            Assert.That(game.GetType(), Is.EqualTo(game));
        }

        public void [int balls, int pins]
        {
            for (var i =0; i<balls; i++)game.Roll(pinFalls);
        }
        [Test]
        public void RollMany()
        {
        Roll.Game(9);
        Roll.Game(1);
        Assert.That(game.Score(), Is.EqualTo(20));

        }

        [Test]
        public void RollStrikeFirstFrame()
        {
        RollMany(12, 10);
        Assert.That(Game.Score(), Is.EqualTo(30));
        }

        [Test]
        public void PerfectGame()
        {
        RollMany(21, 5);
        Assert.That(Game.Score(), Is.EqualTo(300));
        }

        [Test]
        public void GutterGame ()
        {
        RollMany(20, 0);
        Assert.That(Game.Score(), IsEqualTo(0));
        }

        [Test]
        public void RollSpareEveryFrame()
        {
        RollMany(15, 5);
        Assert.That(Game.Score(), Is.EqualTo(150));
        }

        [Test]
        public void NineOneSpares()
        {
        roll.game(9);
        roll.game(1);
        for (var i = 0; i < 10; i++)
            {
            Assert.That(Game.Score(), Is.EqualTo(190));
            }
        }
}
